/*
Create the IAM roles and policies required by ECS Service and Tasks
*/

resource "aws_iam_role" "ecs_app_role" {
  name = "ecsExampleTaskRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "ecs_app_role_policy" {
  name        = "ecsExampleTaskPolicy"
  description = "A policy for the Example ECS task"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*",
        "ecr:*",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "ssm:*",
        "secretsmanager:GetSecretValue",
        "kms:Decrypt"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs_app_role_attach" {
  role       = aws_iam_role.ecs_app_role.name
  policy_arn = aws_iam_policy.ecs_app_role_policy.arn
}
