[
  {
    "dnsSearchDomains": null,
    "logConfiguration": {
      "logDriver": "awslogs",
      "secretOptions": null,
      "options": {
        "awslogs-group": "/ecs/${app_name}-${environment}-api",
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "ecs"
      }
    },
    "entryPoint": null,
    "portMappings": [
      {
        "hostPort": ${listen_port},
        "protocol": "tcp",
        "containerPort": ${listen_port}
      }
    ],
    "command": null,
    "linuxParameters": null,
    "cpu": ${cpu},
    "resourceRequirements": null,
    "ulimits": null,
    "dnsServers": null,
    "mountPoints": [],
    "workingDirectory": null,
    "dockerSecurityOptions": null,
    "memory": null,
    "memoryReservation": ${memory},
    "volumesFrom": [],
    "stopTimeout": null,
    "image": "${container_image}",
    "startTimeout": null,
    "dependsOn": null,
    "disableNetworking": null,
    "interactive": null,
    "healthCheck": null,
    "essential": true,
    "links": null,
    "hostname": null,
    "extraHosts": null,
    "pseudoTerminal": null,
    "user": null,
    "readonlyRootFilesystem": null,
    "dockerLabels": null,
    "systemControls": null,
    "privileged": null,
    "name": "${app_name}-${environment}-api"
  }
]
