/*
Outputs from the 'ecs-service' module
*/

output "ecs_service_tg" {
  value = aws_lb_target_group.ecs_service_tg.arn
}
